import gulp from 'gulp';
import plumber from 'gulp-plumber';
import concat from 'gulp-concat';
import sourcemaps from 'gulp-sourcemaps';

import watch from 'gulp-watch';

import pug from 'gulp-pug';
import sass from 'gulp-sass';
import postcss from 'gulp-postcss';
import autoprefixer from 'autoprefixer';

import babel from 'gulp-babel';

import browserSync from 'browser-sync';

const server = browserSync.create();

gulp.task('pug', ()=>
  gulp.src('./dev/pug/02_pages/*.pug')
    .pipe(plumber())
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest('./public/'))
);

gulp.task('sass', ()=>
  gulp.src('./dev/scss/styles.scss')
    .pipe(sourcemaps.init({
      loadMaps: true
    }))
    .pipe(plumber())
    .pipe(sass({
      outputStyle: 'condensed'
    }))
    .pipe(postcss([
      autoprefixer({
        browsers: ['last 1 version']
      })
    ]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./public/css/'))
    .pipe(server.stream({
      match: '**/*.css'
    }))
);

gulp.task('babel', ()=>
  gulp.src('./dev/js/app.js')
    .pipe(plumber())
    .pipe(babel())
    .pipe(concat('app.js'))
    .pipe(sourcemaps.init({
      loadMaps: true
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./public/js'))
);

gulp.task('default', ()=>{
  server.init({
    server: {
      baseDir: './public'
    },
  });

  watch('./dev/scss/**/*.scss', ()=> gulp.start('sass'));
  watch('./dev/pug/**/*.pug', ()=> gulp.start('pug', server.reload));
  watch('./dev/js/**/*.js', ()=> gulp.start('babel', server.reload));
})