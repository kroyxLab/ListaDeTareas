//definiendo funciones basicas
const $ = selector => document.querySelectorAll(selector)>1? document.querySelectorAll(selector) : document.querySelector(selector);


//Definiendo clases
class Task{
  constructor(name){
    this.name = name;
    this.isComplete = false;
  }
  complete(){
    this.isComplete = !this.isComplete;
  }
}

class TaskList{
  constructor(name){
    this.name = name;
    this.tasks = []
  }

  addTask(task){
    this.tasks.push(task);
  }

  deleteTask(index){
    this.tasks.splice(index, 1);
  }

  render(element){
    let tasks = this.tasks.map(task =>`
    <li class="taskItem ${task.isComplete ? 'taskItem--complete' : ''}">
      <label class="taskItem__checkContainer">
        <input class="taskItem__checkbox" type="checkbox" ${task.isComplete ? 'checked' : ''}>
        <span class="taskItem__completed"></span>
      </label>
      <p class="taskItem__title">${task.name}</p>
      <a class="taskItem__delete" href="#">X</a>
    </li>
    `);
    
    element.innerHTML = tasks.reduce((a,b)=> a+b,'');
  }
}


//Trabajar con el DOM
const addTaskElement = $('.addTask__add');
const taskContainer = $('.listTask');
const inbox = new TaskList('inbox');

// agregar tarea desde el dom

function addDOMTask(e, list = inbox){
  //obtener el texto del input
  if(e.key === 'Enter'){
    //crear la tarea instanciando la clase
    let task = new Task(this.value);
    //agregar la tarea a la lista
    list.addTask(task);
    list.render(taskContainer);

    this.value = '';
  }
}


function getIndex(e){
  let taskItem = e.target.parentElement,
      tasksItems = [...taskContainer.querySelectorAll('li')];
  return tasksItems.indexOf(taskItem);
}


// eliminar tarea
function removeDOMTask(e, list = inbox){
  //detectar que se hizo click en el enlace
  if(e.target.tagName === 'A'){
    //remover tarea de la lista (se necesita el indice)
    list.deleteTask(getIndex(e));
    list.render(taskContainer);
  }
}

//completar tarea
function completeDOMTask(e, list = inbox){
  //detectar que se hizo click en el span
  if(e.target.tagName === 'SPAN'){
    //completar la tarea de la lista (se necesita el indice)
    let taskItem = e.target.parentElement.parentElement,
      tasksItems = [...taskContainer.querySelectorAll('li')],
      i = tasksItems.indexOf(taskItem);

    list.tasks[i].complete();

    e.target.parentElement.parentElement.classList.toggle('taskItem--complete');
  }
}


addTaskElement.addEventListener('keyup', addDOMTask);
taskContainer.addEventListener('click', removeDOMTask);
taskContainer.addEventListener('click', completeDOMTask);
